Source: kpatch
Section: kernel
Priority: optional
Maintainer: Ubuntu Kernel Team <kernel-team@lists.ubuntu.com>
Uploaders:
 Dimitri John Ledkov <dimitri.ledkov@canonical.com>,
 Emmanuel Arias <eamanu@debian.org>,
 Santiago Ruano Rincón <santiago@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 g++-14 [ppc64el],
 gcc-14-plugin-dev [ppc64el],
 libelf-dev,
 shellcheck,
Standards-Version: 4.7.0.1
Homepage: https://github.com/dynup/kpatch
Vcs-Browser: https://salsa.debian.org/debian/kpatch
Vcs-Git: https://salsa.debian.org/debian/kpatch.git

Package: kpatch
Architecture: amd64 ppc64el s390x
Replaces:
 kpatch-dkms,
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Runtime tools for Kpatch
 kpatch is a Linux dynamic kernel patching tool which allows you to patch a
 running kernel without rebooting or restarting any processes.  It enables
 sysadmins to apply critical security patches to the kernel immediately, without
 having to wait for long-running tasks to complete, users to log off, or
 for scheduled reboot windows.  It gives more control over up-time without
 sacrificing security or stability.

Package: kpatch-build
Architecture: amd64 ppc64el s390x
Depends:
 asciidoc,
 autoconf,
 automake,
 bc,
 bison,
 bzip2,
 cpio,
 debhelper (>= 10),
 devscripts,
 docbook-utils,
 dpkg-dev,
 elfutils,
 flex,
 gawk,
 ghostscript,
 kernel-wedge,
 kmod,
 libaudit-dev,
 libdw-dev,
 libelf-dev,
 libiberty-dev,
 libnewt-dev,
 libpci-dev,
 libssl-dev,
 libtool,
 libudev-dev,
 makedumpfile,
 openssl,
 pahole,
 pkgconf,
 python3-dev,
 rsync,
 sharutils,
 transfig,
 uuid-dev,
 xmlto,
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 ccache,
Description: Build Tools for Kpatch and Livepatch
 kpatch-build is a tool that can build both kpatch and livepatch modules from
 a given patch.
